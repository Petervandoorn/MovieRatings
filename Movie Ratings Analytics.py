# coding: utf-8

# In[150]:


import pandas as pd
import os


# In[151]:


os.getcwd()


# In[152]:


movies = pd.read_csv("Dataset.csv")


# In[153]:


len(movies)


# In[154]:


movies.head()


# In[155]:


movies.columns


# In[156]:


movies.columns = ['Film', 'Genre', 'CriticRating', 'AudienceRating','BudgetMillions', 'Year']


# In[157]:


movies.info()


# In[158]:


movies.describe()


# In[159]:


#changing  datatype from integer to category
movies.Film = movies.Film.astype('category')


# In[160]:


movies.head()


# In[161]:


movies.info()


# In[162]:


movies.Genre = movies.Genre.astype('category')
movies.Year = movies.Film.astype('category')


# In[163]:


from matplotlib import pyplot as plt
import seaborn as sns
get_ipython().magic('matplotlib inline')
import warnings
warnings.filterwarnings('ignore')


# In[164]:


movies.info()


# In[165]:


movies.Genre.cat.categories


# In[166]:


movies.describe()


# # jointplots

# In[167]:


#joint plot


# In[185]:


j = sns.jointplot(data=movies, x='CriticRating', y='AudienceRating')


# In[186]:


j = sns.jointplot(data=movies, x='AudienceRating', y='CriticRating', kind = "hex" )


# # histograms

# In[187]:


#already know:
ml = sns.distplot(movies.AudienceRating, bins=15)


# In[188]:


#sns.set_style("darkgrid")
n1 = plt.hist(movies.AudienceRating, bins=15)


# In[189]:


n1 = plt.hist(movies.CriticRating, bins=15)


# In[190]:


#stacked histogram


# In[191]:


movies.head()


# In[192]:


plt.hist(movies[movies.Genre == 'Action'].BudgetMillions, bins=15)
plt.hist(movies[movies.Genre == 'Drama'].BudgetMillions, bins=15)
plt.hist(movies[movies.Genre == 'Thriller'].BudgetMillions, bins=15)


plt.show()


# In[193]:


plt.hist([movies[movies.Genre == 'Drama'].BudgetMillions,          movies[movies.Genre == 'Action'].BudgetMillions], bins=15, stacked=True)
plt.show


# In[194]:


#up top we saw that movies.Genre.cat.categories will give us a list of all categories
#we can use that knowledge for our for function, to iterate over. 
# For every item in movies.Genre.cat.categories a value is printed:
for gen in movies.Genre.cat.categories:
    print(gen)


# In[195]:


#We will create an empty list list1, and then append to that list

list1 = []
mylabels = list()
for gen in movies.Genre.cat.categories:
    list1.append(movies[movies.Genre == gen].BudgetMillions)
    mylabels.append(gen)
    
h= plt.hist(list1, bins=30, stacked=True, rwidth=1,label=mylabels)
plt.legend()
plt.show()


# In[196]:


#KDE Plot


# In[197]:


vis1 = sns.lmplot(data=movies, x='CriticRating', y='AudienceRating',                  fit_reg=False, hue= 'Genre',                   size=7, aspect=1)


# In[198]:


k1 = sns.kdeplot(movies.CriticRating, movies.AudienceRating,                 shade=True, shade_lowest=False, cmap='Reds',               )
 
#tip by running the code twice bur removing the shade you can combine
#the color of 1 plot and have the border of the second plot
#the result is a much cleaner line border 

kb1 = sns.kdeplot(movies.CriticRating, movies.AudienceRating,                 shade_lowest=False, cmap='Reds',               )


# In[182]:


#working with subplots


# In[183]:


import seaborn as sns
sns.set_style("dark")
k1 = sns.kdeplot(movies.BudgetMillions, movies.AudienceRating)


# In[184]:


k2 = sns.kdeplot(movies.BudgetMillions, movies.CriticRating)


# In[62]:


f, ax = plt.subplots(1,2)
#Creates 2 empty plots


# In[63]:


f, axes = plt.subplots(1,2, figsize=(12,6), sharex=True, sharey=True)
k1 = sns.kdeplot(movies.BudgetMillions, movies.AudienceRating, ax=axes[0])
k2 = sns.kdeplot(movies.BudgetMillions, movies.CriticRating, ax=axes[1])
k1.set(xlim=(-20,160))



#if you have more than 1 column or more than 1 row of subplots
#then the variable Axes will be a 2 dimensional array
#you will need to specify the place of the graph like: ax=axes[0,0] 


# In[64]:


#Violinplots vs. Boxplots


# In[65]:


z = sns.violinplot(data=movies, x='Genre', y='CriticRating')


# In[66]:


f, axes = plt.subplots(1,2, figsize=(12,6), sharex=True, sharey=True)
z = sns.violinplot(data=movies, x='Genre', y='CriticRating', ax=axes[0])
z1 = sns.boxplot (data=movies, x='Genre', y='CriticRating', ax=axes[1])
k1.set(xlim=(-20,160))


# In[67]:


w1 = sns.boxplot(data=movies[movies.Genre=='Drama'], x='Year', y='CriticRating')


# In[68]:


w1 = sns.violinplot(data=movies[movies.Genre=='Drama'], x='Year', y='CriticRating')


# In[69]:


#Create a facet Grid


# In[70]:


g = sns.FacetGrid(movies, row='Genre', col='Year', hue='Genre')
kws = dict(s=50, linewidth=0.5, edgecolor='Black')
g = g.map(plt.scatter,'CriticRating', 'AudienceRating', **kws)


# In[71]:


kws


# In[72]:


#Diagonals & Coordinates


# In[75]:


#Controlling axis and adding diagonals
g = sns.FacetGrid(movies, row='Genre', col='Year', hue='Genre')
kws = dict(s=50, linewidth=0.5, edgecolor='Black')
g = g.map(plt.scatter,'CriticRating', 'AudienceRating', **kws)
g.set(xlim=(0,100),ylim=(0,100))
for ax in g.axes.flat:
    ax.plot((0,100),(0,100), c="grey", ls='--')
    
g.add_legend()


# ### Dashboards

# In[87]:


from matplotlib import pyplot as plt
import seaborn as sns
get_ipython().magic('matplotlib inline')
import warnings
warnings.filterwarnings('ignore')


# In[145]:


sns.set_style("darkgrid")
f, axes = plt.subplots(3,2, figsize=(15,15))
k1 = sns.kdeplot(movies.BudgetMillions, movies.AudienceRating, ax=axes[0,0])
k2 = sns.kdeplot(movies.BudgetMillions, movies.CriticRating, ax=axes[0,1])
k3 = sns.violinplot(data=movies[movies.Genre=='Drama'], x='Year', y='CriticRating',ax=axes[1,0])
k4 = sns.kdeplot(movies.CriticRating, movies.AudienceRating,                 shade=True, shade_lowest=False, cmap='Reds',                 ax=axes[2,1]
               )
kb4 = sns.kdeplot(movies.CriticRating, movies.AudienceRating,                 shade_lowest=False, cmap='Reds',                  ax=axes[2,1]
               )
ax=axes[2,0].hist(movies.CriticRating, bins=15)

k61 = sns.boxplot(data=movies[movies.Genre=='Drama'], x='Year', y='CriticRating',ax=axes[1,1])


k1.set(xlim=(-20,160))
k2.set(xlim=(-20,160))
plt.show()


# ### Styling dashboards

# In[149]:


sns.set_style("dark", {"axes.facecolor": "Black"}) 
f, axes = plt.subplots(3,2, figsize=(15,15))


#plot [0,0]
k1 = sns.kdeplot(movies.BudgetMillions, movies.AudienceRating,                  shade=True, shade_lowest=True, cmap='inferno',                    ax=axes[0,0])

kb1 = sns.kdeplot(movies.BudgetMillions, movies.AudienceRating,                    cmap='cool',                    ax=axes[0,0]
               )

#plot [0,1]
k2 = sns.kdeplot(movies.BudgetMillions, movies.CriticRating,                  shade=True, shade_lowest=True, cmap='inferno',                    ax=axes[0,1])

kb2 = sns.kdeplot(movies.BudgetMillions, movies.CriticRating,                    cmap='cool',                    ax=axes[0,1])

#plot [1,0]
k3 = sns.violinplot(data=movies, x='Year', y='BudgetMillions',ax=axes[1,0],                   palette='YlOrRd')




#plot [1,1]
k4 = sns.kdeplot(movies.CriticRating, movies.AudienceRating,                 shade=True, shade_lowest=False, cmap='Blues_r',                 ax=axes[1,1]
               )
kb4 = sns.kdeplot(movies.CriticRating, movies.AudienceRating,                 shade_lowest=False, cmap='gist_gray_r',                  ax=axes[1,1]
               )




#plot [2,0]
ax=axes[2,0].hist(movies.CriticRating, bins=15)



#plot [2,1]
k6 = sns.boxplot(data=movies[movies.Genre=='Drama'], x='Year', y='CriticRating',ax=axes[2,1])






k1.set(xlim=(-20,160))
k2.set(xlim=(-20,160))
plt.show()


# ### Advanced styling

# In[221]:


list1 = []
mylabels = list()
for gen in movies.Genre.cat.categories:
    list1.append(movies[movies.Genre == gen].BudgetMillions)
    mylabels.append(gen)
    
    
sns.set_style("whitegrid")
fig, ax = plt.subplots()
fig.set_size_inches(11.7 , 8.27)
h= plt.hist(list1, bins=30, stacked=True, rwidth=1,label=mylabels)
plt.title("Movies Budget Distribution", fontsize='40',          color="DarkBlue", fontname='Console')

plt.ylabel("Number of movies", fontsize='26', color='red')
plt.xlabel("Budget", fontsize='26', color='Green')
plt.yticks(fontsize=20)
plt.xticks(fontsize=20)
plt.legend(frameon=True, framealpha=1, fancybox=True,shadow=True, prop={'size':20})
plt.show()

#By adding a subplot we can manipulate the behavior of the entire plot Like setting the size

